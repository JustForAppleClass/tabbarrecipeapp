//
//  IngredientsText.h
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IngredientsText : UITextView

-(instancetype)init;

@end

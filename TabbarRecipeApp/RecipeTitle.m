//
//  RecipeTitle.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "RecipeTitle.h"

@implementation RecipeTitle

-(instancetype)init{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    self = [super initWithFrame:CGRectMake(20, screenHeight/20, screenWidth - 40, screenWidth/10)];
    self.backgroundColor = [UIColor PrimaryOrangeColor];
    self.textColor = [UIColor whiteColor];
    self.textAlignment = NSTextAlignmentCenter;
                                            return self;
}


@end

//
//  TableViewController.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor PrimaryOrangeColor];
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"RecipeList" ofType:@"plist"];
    NSArray* arrayofRecipes = [[NSArray alloc]initWithContentsOfFile:filepath];
    self.recipeArr =[[NSMutableArray alloc]init];
    for (int i =0; i < arrayofRecipes.count; i++) {
        NSDictionary* temp = arrayofRecipes[i];
        Recipe* oneRec = [Recipe new];
        oneRec.ingredients = temp[@"Ingredients"];
        oneRec.directions = temp[@"Instructions"];
        oneRec.image = temp[@"image"];
        oneRec.name = temp[@"name"];
        self.recipeArr[i] = oneRec;
        
    }
    UITableView* table = [[UITableView alloc]initWithFrame:self.view.frame];
    table.delegate =self;
    table.dataSource = self;
    [self.view addSubview:table];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" ];
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    Recipe* oneRec =self.recipeArr[indexPath.row];
    cell.textLabel.text = oneRec.name;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.tabBarController setSelectedIndex:1];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Recipe" object:self.recipeArr[indexPath.row]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

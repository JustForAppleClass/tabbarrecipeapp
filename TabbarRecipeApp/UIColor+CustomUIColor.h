//
//  UIColor+CustomUIColor.h
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomUIColor)

+(UIColor*)PrimaryOrangeColor;

+(UIColor*)PrimaryPurpleColor;

@end

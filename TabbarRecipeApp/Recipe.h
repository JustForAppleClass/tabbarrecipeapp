//
//  Recipe.h
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject


@property(nonatomic, strong) NSString* name;

@property(nonatomic, strong) NSString* ingredients;
@property(nonatomic, strong) NSString* directions;
@property(nonatomic, strong) NSString* image;

@end

//
//  DirectionsText.m
//  TabbarRecipeApp
//
//  Created by Michelle Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "DirectionsText.h"

@implementation DirectionsText

-(instancetype)init
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat width = screenRect.size.width;
    CGFloat height = screenRect.size.height;
    self = [super initWithFrame:CGRectMake(20, (height/5)*4 -40, (width - 40), 100)];
    self.textColor = [UIColor whiteColor];
    self.backgroundColor =[UIColor PrimaryOrangeColor];
    self.editable = NO;
    return self;
}

@end

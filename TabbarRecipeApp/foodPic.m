//
//  foodPic.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "foodPic.h"

@implementation foodPic

-(instancetype)init{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat width = screenRect.size.width;
    CGFloat height = screenRect.size.height;
    self = [super initWithFrame:CGRectMake(20, (height/7), width - 40, height/2.5)];
    self.contentMode = UIViewContentModeScaleAspectFit;
    return self;
}

@end

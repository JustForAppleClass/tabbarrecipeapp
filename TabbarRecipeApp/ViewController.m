//
//  ViewController.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    TableViewController* tvc = [TableViewController new];
    RecipeViewController*   rvc = [RecipeViewController new];
    
    NSArray* ViewControllers = @[tvc, rvc];
    
    [self setViewControllers:ViewControllers];
    
    tvc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipe Select" image:nil tag:0];
    
    rvc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipe" image:nil tag:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  DirectionsText.h
//  TabbarRecipeApp
//
//  Created by Michelle Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectionsText : UITextView

-(instancetype)init;

@end

//
//  RecipeViewController.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Set BackgroundColor
    self.view.backgroundColor =[UIColor blackColor];
    
    //Set up the the notificationcenter
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationRecieved:) name:@"Recipe" object:nil];
   
    
    //Make Recipe name label
    self.lbl = [[RecipeTitle alloc]init];
    
    
    
    //Make Ingredients block
    self.ingredients = [[IngredientsText alloc]init];
    
    //Make Directions block
    self.directions = [[DirectionsText alloc]init];
    
    //Make an image for the food picture
    self.food = [[foodPic alloc]init];
    
    
    

}

-(void)NotificationRecieved:(NSNotification*)n
{
    //Grab the object from the notification
    Recipe* temp = n.object;
    
    //Set all of the display objects to have text or an image
    self.lbl.text = temp.name;
    self.ingredients.text = temp.ingredients;
    self.directions.text= temp.directions;
    [self.food setImage:[UIImage imageNamed:temp.image]];
    
    //add all of the display objects to the view
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.ingredients];
    [self.view addSubview:self.directions];
    [self.view addSubview:self.food];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

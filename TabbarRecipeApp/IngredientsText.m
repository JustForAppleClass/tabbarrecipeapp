//
//  IngredientsText.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/5/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "IngredientsText.h"

@implementation IngredientsText

-(instancetype)init
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat width = screenRect.size.width;
    CGFloat height = screenRect.size.height;
    self = [super initWithFrame:CGRectMake(20, ((height/4)*3) - 120 , (width - 40), 100)];
    self.backgroundColor =[UIColor PrimaryPurpleColor];
    self.textColor = [UIColor whiteColor];
    self.editable = NO;
    return self;
}

@end

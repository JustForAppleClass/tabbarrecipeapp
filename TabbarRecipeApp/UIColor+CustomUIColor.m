//
//  UIColor+CustomUIColor.m
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "UIColor+CustomUIColor.h"

@implementation UIColor (CustomUIColor)

+(UIColor*)PrimaryOrangeColor {
    return [UIColor colorWithRed:1.00 green:0.36 blue:0.00 alpha:1.0];
    
}

+(UIColor*)PrimaryPurpleColor{
    return [UIColor colorWithRed:0.81 green:0.00 blue:0.81 alpha:1.0];
}

@end

//
//  RecipeViewController.h
//  TabbarRecipeApp
//
//  Created by Matthew Griffin on 6/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeViewController : UIViewController

@property(nonatomic, strong)RecipeTitle* lbl;
@property(nonatomic, strong)IngredientsText* ingredients;
@property(nonatomic, strong)DirectionsText* directions;
@property(nonatomic, strong)foodPic* food;


@end
